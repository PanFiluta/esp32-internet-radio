#include "Arduino.h"
#include "WiFi.h"
#include "Audio.h"

// I2S piny pro audio
#define I2S_DOUT 25
#define I2S_BCLK 27
#define I2S_LRC 26

// Piny pro enkodér
#define ENCODER_CLK 32
#define ENCODER_DT 33
#define ENCODER_SW 34

Audio audio;
volatile int volume = 10; // Počáteční hlasitost
volatile bool volumeChanged = false; // Indikátor změny hlasitosti

const char* ssid = "ssid";
const char* password = "password";

void IRAM_ATTR onEncoderTurn() {
  static uint8_t lastStateCLK = HIGH;
  uint8_t currentStateCLK = digitalRead(ENCODER_CLK);
  if (currentStateCLK != lastStateCLK){ // Pokud došlo ke změně
    if (digitalRead(ENCODER_DT) != currentStateCLK) {
      volume++;
    } else {
      volume--;
    }
    volume = constrain(volume, 0, 30); // Omezení rozsahu hlasitosti
    volumeChanged = true;
  }
  lastStateCLK = currentStateCLK; // Uložení aktuálního stavu pro porovnání příště
}

void setup() {
  Serial.begin(9600);
  
  // Nastavení pinů enkodéru
  pinMode(ENCODER_CLK, INPUT_PULLUP);
  pinMode(ENCODER_DT, INPUT_PULLUP);
  pinMode(ENCODER_SW, INPUT_PULLUP);

  // Nastavení přerušení pro enkodér
  attachInterrupt(digitalPinToInterrupt(ENCODER_CLK), onEncoderTurn, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ENCODER_DT), onEncoderTurn, CHANGE);

  // Nastavení WiFi
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("\nConnected to WiFi");

  // Nastavení audio výstupu
  audio.setPinout(I2S_BCLK, I2S_LRC, I2S_DOUT);
  audio.setVolume(volume); // Nastavení počáteční hlasitosti

  // Připojení k rádiové stanici
  if (!audio.connecttohost("http://icecast1.play.cz/kiss128.mp3")) {
    Serial.println("Connection to host failed");
  } else {
    Serial.println("Connected to host");
  }
}

void loop() {
  audio.loop();
  
  if (volumeChanged) { // Pokud došlo ke změně hlasitosti
    audio.setVolume(volume);
    Serial.printf("Volume: %d\n", volume);
    volumeChanged = false;
  }
  
  // Kontrola stisku tlačítka enkodéru
  static bool lastButtonState = HIGH;
  bool currentButtonState = digitalRead(ENCODER_SW);
  if (currentButtonState != lastButtonState) {
    lastButtonState = currentButtonState; // Aktualizace posledního stavu
    if (currentButtonState == HIGH) {
      // Tlačítko bylo uvolněno
      Serial.println("Encoder button pressed!");
      delay(300); // Jednoduché odstranění zakmitů (debouncing)
    }
  }
}
